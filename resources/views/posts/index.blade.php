@extends('layout')

@section('title', 'posts')

@section('body')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Homepage</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Posts</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-3">
                <a href="/posts/create" type="button" class="btn btn-outline-primary">Add</a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-8">
                <table class="table table-light">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Slug</th>
                        <th scope="col">Body</th>
                        <th scope="col">Category</th>
                        <th scope="col">Tags</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <th scope="row">{{ $post->id }}</th>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->slug }}</td>
                            <td>{{ $post->body }}</td>
                            <td>{{ $post->category->title }}</td>
                            <td>@foreach($post->tags as $tag) {{ $tag->title . ' ' }} @endforeach</td>
                            <td>
                                <a href="/posts/update/{{ $post->id }}">Edit</a> |
                                <a href="/posts/delete/{{ $post->id }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
@endsection
