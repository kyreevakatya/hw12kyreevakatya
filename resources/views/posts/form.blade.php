@extends('layout')

@section('title', 'posts_form')

@section('body')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-4">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Homepage</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Form</li>
                        <li class="breadcrumb-item"><a href="/posts">Back</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-4">
                <form method="post">
                    <div class="mb-3">
                        <label for="title" class="form-label">Title</label>
                        <input type="text" name="title" class="form-control" id="title"
                               @isset($post) value="{{ $post->title }}" @endisset>
                    </div>
                    <div class="mb-3">
                        <label for="slug" class="form-label">Slug</label>
                        <input type="text" name="slug" class="form-control" id="slug"
                               @isset($post) value="{{ $post->slug }}" @endisset>
                    </div>
                    <div class="mb-3">
                        <label for="slug" class="form-label">Body</label>
                        <input type="text" name="body" class="form-control" id="body"
                               @isset($post) value="{{ $post->body }}" @endisset>
                    </div>
                    <div>
                        <div class="mb-3">
                            <label for="categories" class="form-label">Categories</label>
                            <select class="form-select" name="category_id" aria-label="Default select example">
                                <option selected>Select a category</option>

                                @foreach($categories as $category)
                                    <option value="{{$category->id}}"
                                            @isset($post)
                                            @if($post->category_id == $category->id) selected @endif @endisset>{{$category->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="tags" class="form-label">Tags</label>
                            <select class="form-select" multiple name="tags[]" aria-label="multiple select example">
                                @foreach($tags as $tag)
                                    <option value="{{$tag->id}}"
                                            @isset($post)
                                            @foreach ($post->tags as $postTag)
                                            @if($postTag->id == $tag->id) selected @endif
                                            @endforeach
                                            @endisset
                                    >{{$tag->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        @isset($post)
                            <input type="hidden" name="id" value="{{ $post->id }}">
                        @endisset
                        <input type="submit" class="btn btn-primary" value="Save"/>
                    </div>
                </form>
            </div>
            </form>
        </div>
    </div>
    </div>
@endsection
